const express = require('express');
const bodyParser = require('body-parser');
const asyncHandler = require('express-async-handler');
const request = require('request-promise');

const app = express();
const PORT = process.env.PORT || 8000;

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

app.get('/', (req, res) => res.send("Send POST request to /incoming-messages"));

var conversationId = {};
var bearerToken = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJpS216TTVkenRIWmprdmdSY3VrVHgxTzJ2SFlTM0U5YmVJME9XbXRNR1ZzIn0.eyJqdGkiOiJjNjcyNmVlNC01ZTM5LTRiMTMtYTA4Mi02NGI2NzcyMTBlYTEiLCJleHAiOjE4ODY3MjI0ODgsIm5iZiI6MCwiaWF0IjoxNTcxMzYyNDg4LCJpc3MiOiJodHRwOi8vaW50ZXJuYWwtZmMtdXNlMS0wMC1rZXljbG9hay1vYXV0aC0xMzA3MzU3NDU5LnVzLWVhc3QtMS5lbGIuYW1hem9uYXdzLmNvbS9hdXRoL3JlYWxtcy9wcm9kdWN0aW9uIiwiYXVkIjoiMTU0NmJhZmQtN2Y3Ni00NTY2LTlmYTEtMmIxOGM0ZDcwMWNjIiwic3ViIjoiN2E0ZDczNDUtNDUwMy00YzRiLWIyYmItN2EzN2ZiMTQ5MDI4IiwidHlwIjoiQmVhcmVyIiwiYXpwIjoiMTU0NmJhZmQtN2Y3Ni00NTY2LTlmYTEtMmIxOGM0ZDcwMWNjIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiOGFkZDFkYmItZGM3Mi00ODBiLWFiYTYtN2RkYTM3MzVkM2VkIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6W10sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJhZ2VudDp1cGRhdGUgbWVzc2FnZTpjcmVhdGUgYWdlbnQ6Y3JlYXRlIGRhc2hib2FyZDpyZWFkIHJlcG9ydHM6cmVhZCBhZ2VudDpyZWFkIGNvbnZlcnNhdGlvbjp1cGRhdGUgdXNlcjpkZWxldGUgY29udmVyc2F0aW9uOmNyZWF0ZSBvdXRib3VuZG1lc3NhZ2U6Z2V0IG91dGJvdW5kbWVzc2FnZTpzZW5kIHVzZXI6Y3JlYXRlIHJlcG9ydHM6ZmV0Y2ggdXNlcjp1cGRhdGUgdXNlcjpyZWFkIGNvbnZlcnNhdGlvbjpyZWFkIiwiY2xpZW50SG9zdCI6IjE5Mi4xNjguMTI4LjE2NiIsImNsaWVudElkIjoiMTU0NmJhZmQtN2Y3Ni00NTY2LTlmYTEtMmIxOGM0ZDcwMWNjIiwiY2xpZW50QWRkcmVzcyI6IjE5Mi4xNjguMTI4LjE2NiJ9.SJDpGHJ-2x-tvZ73f2stRNPtzj9urJc43xv0_UiWfewIJ3eenWDfoe1ExkgJO5XQjvDR0_TK4cs4XfHk4PlEajyIIpXJOixKsJPfiW6BZv8IiopKH-VOAUUFDe-I_8KxhfRy8CY0jFJiSddUS5g7kFtf_0VE60KLvMSh-0oFnG5DslQmF37hLTLQp4wsvGhKxkhI_RyfqzS8pST41URh2nHwNzaidzpXnDuLnkQYJq85T6KpAG7zsKA4LTGpR47UEOFbOFAoek8nr_Ot2PJXp6_X44IF_LtTMmBAJ-LUwyz1QkDzhV9_ZmmL8mq4Awuz93csEh0wZzFZljeBhGYhlQ";

app.post('/incomingMessage', asyncHandler(async (req, res, next) => {
    try {
        const messageObject = req.body.data.message;

        console.log("This is the userId: " + messageObject.actor_id);
        console.log("This is the conversationId: " + messageObject.conversation_id);
        conversationId = messageObject.conversation_id;

        if (messageObject.actor_type == "user") {

            incomingMessage = constructIncomingMessage(messageObject);

            const botOptions = {
                method: "POST",
                uri: "https://kanal.kata.ai/receive_message/fda53a2c-3dd2-44cc-bd9a-7103216510e0",
                body: {
                    userId: messageObject.actor_id,
                    messages: incomingMessage
                },
                json: true
            }

            const resultBot = await request(botOptions);

            res.send({
                resultBot
            });
        } else {
            console.log("actor_type is not user");

            res.send({
                status: "actor_type is not user"
            });
        }

    } catch (err) {
        console.log(err);
        res.send({ 
            status: "error",
            error: err
        });
    }
}));

app.post('/outgoingMessage', asyncHandler(async (req, res, next) => {
    try {

        const messageObject = req.body;

        console.log("This is messageObject: " + messageObject);

        const { outgoingMessages, outgoingQuickReplies } = constructOutgoingMessage(messageObject);
        
        const chatOptions = {
            method: "POST",
            uri: `https://api.freshchat.com/v2/conversations/${conversationId}/messages`,
            headers: {
                Authorization: `Bearer ${bearerToken}`
            },
            body: {
                actor_type: "agent",
                actor_id: "50bb7b8c-64a4-4e17-a06c-4f68b0e7f369",
                message_type: "normal",
                message_parts: outgoingMessages,
                reply_parts: [{
                    collection: {
                        sub_parts: outgoingQuickReplies
                    }
                }]
            },
            json: true
        }

        const resultChat = await request(chatOptions);

        console.log(resultChat);

        res.send({
            resultChat
        });

    } catch (err) {
        console.log(err);
        res.send({ 
            status: "error",
            error: err
        });
    }
}));

function constructIncomingMessage(message) {
    var incomingMessages = [];
    var incomingMessage = {};

    for (var i = 0, len = message.message_parts.length; i < len; i++) {
        if (message.message_parts[0].text && !message.message_parts[0].image && !message.message_parts[0].url_button) {
            incomingMessage = {
                type: "text",
                content: message.message_parts[0].text.content
            };

            incomingMessages.push(incomingMessage);
        } else if (!message.message_parts[0].text && message.message_parts[0].image && !message.message_parts[0].url_button) {
            incomingMessage = {
                type: "data",
                payload: {
                    type: "template",
                    template_type: "image",
                    items: {
                        originalContentUrl: message.message_parts[0].image.url,
                        previewImageUrl: message.message_parts[0].image.url
                    }
                }
            };

            incomingMessages.push(incomingMessage);
        } else if (!message.message_parts[0].text && !message.message_parts[0].image && message.message_parts[0].url_button) {
            incomingMessage = {
                type: "data",
                payload: {
                    type: "template",
                    template_type: "button",
                    items: {
                        title: message.message_parts[0].url_button.label,
                        text: message.message_parts[0].url_button.label,
                        actions: [{
                            type: "url",
                            label: message.message_parts[0].url_button.label,
                            url: message.message_parts[0].url_button.url,
                        }]
                    }
                }
            };

            incomingMessages.push(incomingMessage);
        }
    }

    return incomingMessages;
}

function constructOutgoingMessage(message) {
    let outgoingMessages = [];
    let outgoingMessage = {};
    let outgoingQuickReplies = [];
    let outgoingQuickReply = {};

    for (var i = 0, len = message.messages.length; i < len; i++) {
        if (message.messages[i].type == "text") {
            outgoingMessage = {
                text: {
                    content: message.messages[i].content
                }
            };
            outgoingMessages.push(outgoingMessage);
        } else if (message.messages[i].type == "data") {
            if (message.messages[i].payload.template_type == "image") {
                outgoingMessage = {
                    image: {
                        url: message.messages[i].payload.items.originalContentUrl
                    }
                };
                outgoingMessages.push(outgoingMessage);
            } else if (message.messages[i].payload.template_type == "button") {
                outgoingMessage = {
                    url_button: {
                        url: message.messages[i].payload.items.actions[0].url,
                        label: message.messages[i].payload.items.actions[0].label
                    }
                };
                outgoingMessages.push(outgoingMessage);
            } else if (message.messages[i].payload.template_type == "text") {
                outgoingMessage = {
                    text: {
                        content: message.messages[i].payload.items.text
                    }
                }
                outgoingMessages.push(outgoingMessage);

                for (var j = 0, leng = message.messages[i].payload.items.quickreply.length; j < leng; j++) {
                    outgoingQuickReply = {
                        quick_reply_button: {
                            label: message.messages[i].payload.items.quickreply[j].label
                        }
                    };
                    outgoingQuickReplies.push(outgoingQuickReply);
                }
            }
        }
    }

    return { outgoingMessages, outgoingQuickReplies };
}

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));